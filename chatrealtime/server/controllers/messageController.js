
const MessageModel = require("../models/messageModel");
module.exports.addMessage = async (req, res, next) => {
  try {
    const { from, to, message } = req.body;
    const data = await MessageModel.create({
      message: {
        text: message,
      },
      user: [from, to],
      sender: from,
    });
    if (data) {
      res.json({ msg: "Message add successfully" });
    }else {
    res.json({ msg: "Failed to add message to db" });
    }
} catch (ex) {
    next(ex);
  }
};
module.exports.getAllMessage = async (req, res, next) => {
  try {
    const { from, to } = req.body;
    const messages = await MessageModel.find({
      users:{
        $all: [from, to]
      }
    }).sort({updatedAt: 1})
    const procjectMessages =  messages.map((msg) => {
      return {
        fromSelf: msg.sender.toString() === from,
        message: msg.message.text
      };
    });
    res.json(procjectMessages)
  } catch (ex) {
    next(ex);
    
  }
};
