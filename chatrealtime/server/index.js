const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoues = require('./routes/userRoutes');
const messageRoues = require('./routes/messagesRoute')

const app = express();
require('dotenv').config();
app.use(cors());
app.use(express.json());


app.use("/api/auth", userRoues);
app.use("/api/message", messageRoues);


mongoose.connect(process.env.MONGO_URL).then(() => {
    console.log('Connected to Db Success');
}).catch(err => {
    console.error(err.message);
});

const server = app.listen(process.env.PORT, () => {
    console.log(`Server Start on port ${process.env.PORT}`);
})
