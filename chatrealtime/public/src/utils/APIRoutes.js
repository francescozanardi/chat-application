const host =  "http://localhost:5000";
export const RegisterRouter = `${host}/api/auth/register`;
export const LoginRouter = `${host}/api/auth/login`;
export const SetAvatarRoute = `${host}/api/auth/setavatar`;
export const AllUsers = `${host}/api/auth/allusers`;

//message action

export const sendMessage = `${host}/api/message/addmsg`;
export const getAllMessage = `${host}/api/message/getmsg`;